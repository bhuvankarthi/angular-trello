import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrelloCardComponent } from './trello-card.component';

describe('TrelloCardComponent', () => {
  let component: TrelloCardComponent;
  let fixture: ComponentFixture<TrelloCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TrelloCardComponent]
    });
    fixture = TestBed.createComponent(TrelloCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
