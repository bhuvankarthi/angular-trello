import { Component } from '@angular/core';
import { ApiservicesService } from '../services/apiservices.service';
import { Router } from '@angular/router';
import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-trello-card',
  templateUrl: './trello-card.component.html',
  styleUrls: ['./trello-card.component.css'],
})
export class TrelloCardComponent {
  [x: string]: any;
  starredTrello: any[] = [];
  yourTrello: any[] = [];

  getTrello = () => {
    this.apiService.getTrello().subscribe((data) => {
      this.yourTrello = data;
      let starredArray = data.filter((el: any) => {
        return el.starred;
      });
      this.starredTrello = starredArray;
    });
  };
  ngOnInit(): void {
    this.getTrello();
  }

  constructor(
    private apiService: ApiservicesService,
    private route: Router,
    public dialog: MatDialog
  ) {}

  openDialog = () => {
    console.log('hello');
    const dialogRef = this.dialog.open(DialogComponent);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.apiService.addTrelloCard(result).subscribe((data) => {
          console.log(data, 'done');
        });
      }
    });
  };

  setStyle = (value: any): any => {
    let style;
    if (value.prefs.backgroundImage) {
      return (style = {
        'background-image': `url(${value.prefs.backgroundImage})`,
      });
    } else {
      console.log();
      return (style = {
        'background-color': `${value.prefs.backgroundColor}`,
      });
    }
  };

  navigateHandler = (card: any) => {
    console.log(card.id);
    return this.route.navigate(['/trellos', card.id]);
  };

  dataFromModal = (value: any) => {
    this.apiService.addTrelloCard(value).subscribe((data) => {
      console.log(data);
    });
  };
}
