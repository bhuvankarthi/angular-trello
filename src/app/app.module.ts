import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { materialModule } from './materialModel/material.module';
import { ApiservicesService } from './services/apiservices.service';
import { HttpClientModule } from '@angular/common/http';
import { TrelloCardComponent } from './trello-card/trello-card.component';
import { TrellosComponent } from './trellos/trellos.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateTaskComponent } from './create-task/create-task.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DialogComponent } from './dialog/dialog.component';
import { FormsModule } from '@angular/forms';
import { TestComponent } from './test/test.component';
@NgModule({
  declarations: [
    AppComponent,
    TrelloCardComponent,
    TrellosComponent,
    CreateTaskComponent,
    DialogComponent,
    TestComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    materialModule,
    HttpClientModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    FormsModule,
  ],
  providers: [ApiservicesService],
  bootstrap: [AppComponent],
})
export class AppModule {}
