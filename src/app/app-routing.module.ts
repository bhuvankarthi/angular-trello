import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { TrellosComponent } from './trellos/trellos.component';
import { TrelloCardComponent } from './trello-card/trello-card.component';

const routes: Routes = [
  { path: '', redirectTo: '/trellos', pathMatch: 'full' },
  { path: 'trellos', component: TrelloCardComponent },
  { path: 'trellos/:id', component: TrellosComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
