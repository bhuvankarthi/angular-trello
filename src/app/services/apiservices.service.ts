import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiservicesService {
  apiKey: string = '1d29dbddb90494e570f76972c7455a28';
  apiToken: string =
    'ATTAdf900b137c952095ec3aaa87443f1d0aa72530d0555078fd27121a7c2ff655e6B8A5BE1C';
  constructor(private http: HttpClient) {}

  getTrello = (): Observable<any> => {
    return this.http.get(
      `https://api.trello.com/1/members/me/boards?key=${this.apiKey}&token=${this.apiToken}`
    );
  };

  addTrelloCard = (object: any): Observable<any> => {
    return this.http.post(
      `https://api.trello.com/1/boards?name=${object.value}&prefs_background=${object.color}&key=${this.apiKey}&token=${this.apiToken}`,
      {}
    );
  };

  idTrello = (id: any): Observable<any> => {
    return this.http.get(
      `https://api.trello.com/1/boards/${id}/lists?key=${this.apiKey}&token=${this.apiToken}`
    );
  };

  getListOfBoards = (boardId: any): Observable<any> => {
    return this.http.get(
      `https://api.trello.com/1/lists/${boardId}/cards?key=${this.apiKey}&token=${this.apiToken}`
    );
  };

  deleteTrello = (id: any): Observable<any> => {
    return this.http.put(
      `https://api.trello.com/1/lists/${id}/closed?&key=${this.apiKey}&token=${this.apiToken}&value=true`,
      {}
    );
  };
  postToTrello = (value: any, id: any): Observable<any> => {
    return this.http.post(
      `https://api.trello.com/1/boards/${id}/lists?name=${value}%20&key=${this.apiKey}&token=${this.apiToken}`,
      {}
    );
  };

  taskAddToApi = (value: any): Observable<any> => {
    return this.http.post(
      `https://api.trello.com/1/cards?idList=${value.id}&name=${value.data}&key=${this.apiKey}&token=${this.apiToken}`,
      {}
    );
    //
  };

  saveTasks = (object: any): Observable<any> => {
    return this.http.put(
      `https://api.trello.com/1/cards/${object.id}?name=${object.value}&key=${this.apiKey}&token=${this.apiToken}`,
      {}
    );
  };

  deleteTasks = (object: any): Observable<any> => {
    return this.http.delete(
      `https://api.trello.com/1/cards/${object.id}?key=${this.apiKey}&token=${this.apiToken}`
    );
  };
}
