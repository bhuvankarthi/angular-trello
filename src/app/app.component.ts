import { Component, OnInit } from '@angular/core';
import { ApiservicesService } from './services/apiservices.service';
import { Router } from '@angular/router';
import { faHome } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  starredTrello: any[] = [];
  yourTrello: any[] = [];
  faHome: any = faHome;

  getTrello = () => {
    this.apiService.getTrello().subscribe((data) => {
      this.yourTrello = data;
      let starredArray = data.filter((el: any) => {
        return el.starred;
      });
      this.starredTrello = starredArray;
    });
  };
  ngOnInit(): void {
    this.getTrello();
  }

  constructor(private apiService: ApiservicesService, private route: Router) {}
  setStyle = (value: any): any => {
    let style;
    if (value.prefs.backgroundImage) {
      return (style = {
        'background-image': `url(${value.prefs.backgroundImage})`,
      });
    } else {
      console.log();
      return (style = {
        'background-color': `${value.prefs.backgroundColor}`,
      });
    }
  };

  navigateHandler = (card: any) => {
    console.log(card.id);
    return this.route.navigate(['/trellos', card.id]);
  };
  homeNavigation = () => {
    this.route.navigate(['/trellos']);
  };
}
