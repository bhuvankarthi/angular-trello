import { Component, EventEmitter, Output, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
})
export class DialogComponent {
  colors: string[] = ['red', 'green', 'blue', 'yellow', 'orange'];

  color: string = 'violet';
  value: any = '';

  ngcolor: any = {};

  selectColor = (color: string) => {
    this.color = color;
    this.ngcolor = {
      border: '.5px solid black',
    };
  };

  constructor(public dialogRef: MatDialogRef<DialogComponent>) {
    console.log('hello in');
  }

  @Output() modalData = new EventEmitter<any>();

  onOkClick(): void {
    this.dialogRef.close({ value: this.value, color: this.color });
  }
}
