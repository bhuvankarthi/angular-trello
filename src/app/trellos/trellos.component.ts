import { Component, Input, OnInit } from '@angular/core';
import { ApiservicesService } from '../services/apiservices.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faCoffee, faTrash, faEdit } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-trellos',
  templateUrl: './trellos.component.html',
  styleUrls: ['./trellos.component.css'],
})
export class TrellosComponent implements OnInit {
  particularTrello: any = [];
  trelloBoolean: boolean = false;
  toBeOpened: string = '';
  boardList?: any = {};
  keyList: string[] = [];
  faCoffe = faCoffee;
  faTrash = faTrash;
  faEdit = faEdit;
  inputId: any = null;
  loading: boolean = true;
  isEditing: boolean = false;
  keyid: any = null;
  constructor(
    private service: ApiservicesService,
    private active: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  editDelete = this.fb.group({
    value: [''],
  });

  boardListUpdate = (data: any) => {
    if (this.boardList[data.idList]) {
      this.boardList[data.idList].push(data);
    } else {
      this.boardList[data.idList] = [];
      this.boardList[data.idList].push(data);
    }

    this.keyList = Object.keys(this.boardList);
  };

  listOfBoards = (array: any[]) => {
    array.map((trello: any) => {
      this.service.getListOfBoards(trello.id).subscribe((data) => {
        data.map((el: any) => {
          this.boardListUpdate(el);
        });
        this.keyList = Object.keys(this.boardList);
        this.loading = false;
      });
    });
  };
  getTotalTrellos = () => {
    return this.active.paramMap.subscribe((param: ParamMap) => {
      let id = param.get('id');
      this.service.idTrello(id).subscribe((data) => {
        this.particularTrello = data;
        this.listOfBoards(this.particularTrello);
      });
    });
  };

  ngOnInit(): void {
    this.getTotalTrellos();
  }

  addTrelloBoolean = (string: string, id?: any, text?: any) => {
    if (string === 'existing') {
      this.inputId = id;
      this.toBeOpened = string;
    }
    if (string === 'new') {
      this.toBeOpened = string;
    }
    this.trelloBoolean = !this.trelloBoolean;
  };

  addToApi = (value: any) => {
    console.log(value);
    this.trelloBoolean = !this.trelloBoolean;
    if (value.length > 0) {
      console.log(value);
      this.active.paramMap.subscribe((param: ParamMap) => {
        let id = param.get('id');
        return this.service.postToTrello(value, id).subscribe((data) => {
          this.particularTrello.push(data);
        });
      });
    }
  };

  deleteApi = (id: any) => {
    this.service.deleteTrello(id).subscribe((data) => {
      let deletedArray = this.particularTrello.filter((el: any) => {
        this.loading = false;
        return el.id !== data.id;
      });
      this.particularTrello = deletedArray;
    });
  };

  addToTasks = (value: any) => {
    this.service.taskAddToApi(value).subscribe((data) => {
      this.boardListUpdate(data);
    });
  };

  editTask = (id: number, name: any) => {
    this.keyid = id;
    this.isEditing = true;
    this.editDelete.patchValue({
      value: name,
    });
  };

  saveTasks = (value: any) => {
    this.isEditing = false;
    this.keyid = null;
    this.service
      .saveTasks({ value: this.editDelete.value.value, id: value })
      .subscribe((data) => {
        let arrayFiltered = this.boardList[data.idList].filter((el: any) => {
          return el.id !== data.id;
        });
        this.boardList[data.idList] = arrayFiltered;
        this.boardList[data.idList].push(data);
      });
  };
  deleteTasks = (value: any, listItem: any) => {
    this.isEditing = false;
    this.keyid = null;
    this.service
      .deleteTasks({ value: this.editDelete.value.value, id: value })
      .subscribe((data) => {
        let arrayFiltered = this.boardList[listItem].filter((el: any) => {
          return el.id !== value;
        });
        this.boardList[listItem] = arrayFiltered;
      });
  };
}
