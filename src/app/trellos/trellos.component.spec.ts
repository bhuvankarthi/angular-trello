import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrellosComponent } from './trellos.component';

describe('TrellosComponent', () => {
  let component: TrellosComponent;
  let fixture: ComponentFixture<TrellosComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TrellosComponent]
    });
    fixture = TestBed.createComponent(TrellosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
