import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css'],
})
export class CreateTaskComponent implements OnInit {
  @Input() trelloBoolean?: boolean;
  @Input() toBeOpened?: string;
  @Input() id?: any;
  @Input() title?: string;
  ngOnInit(): void {
    console.log('helo');
  }
  constructor(private fb: FormBuilder) {}
  formData = this.fb.group({
    trello: ['', Validators.required],
  });
  @Output() onDatapicked = new EventEmitter<any>();
  @Output() onTaskAdded = new EventEmitter<any>();

  addInput = (title: any) => {
    if (title === 'Add New Trello') {
      this.onDatapicked.emit(this.formData.value.trello);
      this.formData.reset();
    } else {
      this.onTaskAdded.emit({ data: this.formData.value.trello, id: this.id });
      this.formData.reset();
    }
  };
}
